import React, { useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView, Alert } from 'react-native'
import { colorList } from '../constant/colorList';
import axios from 'axios'
import { baseURL } from '../constant/constants';
import Uuid from '../../action/Uuid';


const NewUser = ({navigation}) => {
    const [user, setUser] = useState({
        f_name: '',
        l_name: "",
        age: 0,
        photo: "",
        gender: '',
    })
    const createUser = async () => {
        const number = Math.ceil(Math.random() * 10);

        if (user.gender !== '') {
            const getGender = user.gender.toLowerCase();
            if (getGender === 'men' || getGender === 'women') {
                const photoGenerator = `https://randomuser.me/api/portraits/${getGender}/${number}.jpg`;
                const id = Uuid(24).toString();
                try {
                    await axios.post(baseURL + 'contact',
                        {
                            // "id":id, // gak perlu kirim ID, error
                            "firstName": user.f_name,
                            "lastName": user.l_name,
                            "age": user.age,
                            "photo": photoGenerator,
                        }
                    )
                    .then((result)=>{
                        Alert.alert('SUCCESS',result.data.message)
                        navigation.push('list user')

                    })
                } catch (error) {
                    Alert.alert('FAILED',error.message)
                }
            }
            else {
                alert('invalid gender input')
            }
        }
    }
    
    return (
        <ScrollView style={{ padding: 20, flex: 1 }}>
            <View >

                <Text style={{ textAlign: 'center', fontSize: 24, marginBottom: 24 }}>Create New User</Text>
                <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                    <Text style={{ marginRight: 24, marginTop: 6 }}>First Name: </Text>
                    <TextInput
                        placeholder='first name'
                        autoCapitalize={'none'}
                        onChangeText={(val) => setUser({ ...user, f_name: val })}
                        style={{ borderBottomWidth: 1, width: '100%', paddingTop: -12 }}
                    />
                </View>
                <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                    <Text style={{ marginRight: 24, marginTop: 14 }}>Last Name: </Text>
                    <TextInput
                        placeholder='last name'
                        autoCapitalize={'none'}
                        onChangeText={(val) => setUser({ ...user, l_name: val })}
                        style={{ borderBottomWidth: 1, width: '100%' }}
                    />
                </View>
                <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                    <Text style={{ marginRight: 64, marginTop: 16 }}>Age: </Text>
                    <TextInput
                        placeholder='age'
                        autoCapitalize={'none'}
                        onChangeText={(val) => setUser({ ...user, age: val })}
                        style={{ borderBottomWidth: 1, width: '100%' }}
                    />
                </View>
                <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                    <Text style={{ marginRight: 44, marginTop: 16 }}>Gender: </Text>
                    <TextInput
                        placeholder='men/women'
                        autoCapitalize={'none'}
                        style={{ borderBottomWidth: 1, width: '100%' }}
                        onChangeText={(val) => setUser({ ...user, gender: val })}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => createUser(user.f_name, user.l_name, user.age)}
                    style={styles.btn}>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>POST DATA</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}

export default NewUser

const styles = StyleSheet.create({
    btn: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        borderRadius: 8,
        marginVertical: 36,
        borderWidth: 1,
        backgroundColor: colorList.stone
    }
})