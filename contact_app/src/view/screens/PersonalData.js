import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native'
import { colorList } from '../constant/colorList';
import { baseURL } from '../constant/constants';

const PersonalData = ({ route, navigation }) => {
    const { id } = route.params;
    const [data, setData] = useState({
        id: '',
        firstName: '',
        lastName: '',
        age: null,
        photo: ''
    })
    useEffect(() => {
        getData(id)
    }, [])
    const getData = async (id) => {
        try {
            await axios.get(baseURL + 'contact/' + `${id}`)
                .then((res) => {
                    console.log('=', JSON.stringify(res.data.data, null, 2)),
                        setData({
                            ...data,
                            firstName: res.data.data.firstName,
                            lastName: res.data.data.lastName,
                            age: res.data.data.age.toString(),
                            photo: res.data.data.photo
                        })
                })
                .catch((error) => alert(error.message))
        } catch (error) {
            console.log(error.message)
        }
    }
    const onUpdate = async () => {
        try {
            await axios.put(baseURL + 'contact/' + `${id}`, {
                "firstName": data.firstName,
                "lastName": data.lastName,
                "age": data.age,
                "photo": data.photo
            })
                .then((res) => {
                    alert(res.data.message),
                        navigation.push('list user')
                })
                .catch((err) => alert(err.message))
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <View style={{ padding: 16, flex: 1 }}>
            <Text>The Detail Data - {data.firstName.toUpperCase()}</Text>
            <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                <Text style={{ marginRight: 24, marginTop: 6 }}>First Name: </Text>
                <TextInput
                    placeholder='first name'
                    autoCapitalize={'none'}
                    onChangeText={(val) => setData({ ...data, firstName: val })}
                    style={{ borderBottomWidth: 1, width: '70%', paddingTop: -12 }}
                    value={data.firstName}
                />
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                <Text style={{ marginRight: 24, marginTop: 6 }}>Last Name: </Text>
                <TextInput
                    placeholder='last name'
                    autoCapitalize={'none'}
                    onChangeText={(val) => setData({ ...data, lastName: val })}
                    style={{ borderBottomWidth: 1, width: '70%', paddingTop: -12 }}
                    value={data.lastName}
                />
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                <Text style={{ marginRight: 64, marginTop: 16 }}>Age: </Text>
                <TextInput
                    placeholder='age'
                    autoCapitalize={'none'}
                    onChangeText={(val) => setData({ ...data, age: val })}
                    style={{ borderBottomWidth: 1, width: '100%' }}
                    value={data.age}
                />
            </View>

            <TouchableOpacity
                onPress={() => onUpdate()}
                style={styles.btn}>
                <Text style={{ textAlign: 'center', color: '#fff' }}>Update</Text>
            </TouchableOpacity>
        </View>
    )
}

export default PersonalData

const styles = StyleSheet.create({
    btn: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        borderRadius: 8,
        marginVertical: 26,
        borderWidth: 1,
        backgroundColor: colorList.stone
    },
});