import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Alert, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colorList } from '../constant/colorList'
import { baseURL } from '../constant/constants'

const ListUser = (props) => {
    const [isLoading, setIsLoading] = useState(false)
    const [all, setAll] = useState({});
    useEffect(() => {
        setIsLoading(true),
            getData()
    }, [])
    const getData = async () => {
        try {
            await axios.get(baseURL + 'contact')
                .then((result) => {
                    const all_data = result.data.data;
                    console.log('= ', JSON.stringify(all_data, null, 2))
                    setAll(all_data)
                    setIsLoading(false)
                })
        } catch (error) {
            console.log(error.message)
        }
    }
    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' color={colorList.mist} />
            </View>
        )
    }
    const createUser = () => {
        props.navigation.navigate('new user')
    }

    const onDelete = async (id) => {
        try {
            await axios.delete(baseURL + 'contact/' + `${id}`)
                .then((result) => {
                    alert('success delete data')
                    props.navigation.push('list user')
                })
                .catch((err) => { console.log(err), Alert.alert("FAILED", `${err.message}`,) })
        } catch (error) {
            console.log(error.message)
        }
    }

    const onDetail = (id) => {

        props.navigation.navigate('personal data', { id: id })
    }

    return (
        <View style={{ flex: 1, padding: 20 }}>
            <FlatList
                data={all}
                keyExtractor={item => item.id}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.box}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View>
                                    <Text>Full Name: {item.firstName} {item.lastName}</Text>
                                    <Text>Age: {item.age}</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity
                                            onPress={() => onDelete(item.id)}
                                            style={styles.btn2}>
                                            <Text style={{ textAlign: 'center', color: '#fff', fontSize: 12 }}>Delete</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => onDetail(item.id)}
                                            style={[styles.btn2, { marginLeft: 4, backgroundColor: colorList.shadow }]}>
                                            <Text style={{ textAlign: 'center', color: '#fff', fontSize: 12 }}>Detail</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View >
                                    <Image
                                        source={{ uri: `${item.photo}` }}
                                        style={{ width: 80, height: 54, borderRadius: 14 }}
                                    />
                                </View>
                            </View>
                        </View>
                    )
                }}
            />
            <TouchableOpacity
                onPress={() => createUser()}
                style={styles.btn}>
                <Text style={{ textAlign: 'center', color: '#fff' }}>New User</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ListUser;
const styles = StyleSheet.create({
    box: {
        width: '100%',
        borderWidth: 2,
        borderRadius: 8,
        height: 80,
        borderColor: colorList.stone,
        marginVertical: 6,
        padding: 12
    },
    btn: {
        width: '100%',
        height: 40,
        justifyContent: 'center',
        borderRadius: 8,
        marginVertical: 6,
        borderWidth: 1,
        backgroundColor: colorList.stone
    },
    btn2: {
        width: '28%',
        height: 22,
        justifyContent: 'center',
        borderRadius: 4,
        marginVertical: 3,
        borderWidth: 1,
        backgroundColor: colorList.autumn
    }
})
