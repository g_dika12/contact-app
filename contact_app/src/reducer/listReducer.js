const initialState = {
    firstName: "",
    lastName: "",
    age: 0,
    photo: '',
    isLoading: false
}
const listReducer = (prevState = initialState, action) => {
    switch (action.type) {
        case "LOADING":
            return {
                ...prevState,
                isLoading: true
            }
        case "USER_LIST":
            return {
                ...prevState,
                isLoading: false,
                firstName: action.firstName,
                lastName: action.lastName,
                age: action.age,
                photo: action.photo,
            }
    }
    return prevState;
}
export default listReducer