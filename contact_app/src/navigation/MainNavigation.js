import React from 'react'
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ListUser from '../view/screens/ListUser';
import { colorList } from '../view/constant/colorList';
import NewUser from '../view/screens/NewUser';
import PersonalData from '../view/screens/PersonalData';

const Stack = createStackNavigator();

const MainNavigation = () => {
    const headerStyle = {
        headerStyle: {
            backgroundColor: colorList.stone,
            elevation: 0,
            shadowOpacity: 0
        },
        headerTintColor: '#fff'
    }
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={headerStyle}>
                <Stack.Screen name='list user' component={ListUser} />
                <Stack.Screen name='new user' component={NewUser} />
                <Stack.Screen name='personal data' component={PersonalData}  />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default MainNavigation
