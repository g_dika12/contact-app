import axios from "axios";

export const listAction = (fName, lName, age, photo) => {
    return {
        type: "USER_LIST",
        firstName: fName,
        lastName: lName,
        age: age,
        photo: photo,
        isLoading: false,
    }
}

// export const listController = () => {
//     console.log('run')
//     return async (dispatch) => {
//         try {
//             dispatch(onLoadingProcess());
//             axios.get(
//                 "https://simple-contact-crud.herokuapp.com/contact"
//             )
//             .then((result)=>{
//                 console.log('---+++ > ',JSON.stringify(result, null,2))
//             })
//             .catch((err)=>{console.log(err.message)})
//         } catch (error) {
//             console.log(error.message)
//         }
//     }
// }